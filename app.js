const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const databaseConfig = require('./config/Database');
require('./config/Passport')(passport);

//database
mongoose.connect(databaseConfig.database);
mongoose.connection.on('connected', function () {
    console.log('connected to database');
});
mongoose.connection.on('error', function (err) {
    console.log('database err' + err);
});

//require route
const nguoiDung = require('./routes/NguoiDungRoutes');

const app = express();

const port = 8888;

//Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());


//set static folder
app.use(express.static(path.join(__dirname, 'public')));

//use routes
app.use('/nguoidung', nguoiDung);

//Index Route
app.listen(port, ()=>{
   console.log("123123");
});

app.get('/', (req, res)=>{
    res.send('abc');
});