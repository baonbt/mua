const express = require('express');
const router = express.Router();
const nguoiDung = require('../models/NguoiDung');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const databaseConfig = require('../config/Database');

//register
router.post('/themmoi', (req, res, next)=>{
    let newNguoiDung = new nguoiDung({
       ten: req.body.ten,
       soDienThoai: req.body.soDienThoai,
       taiKhoan: req.body.taiKhoan,
       matKhau: req.body.matKhau,
        ngaySinh: req.body.ngaySinh
    });

    nguoiDung.addNguoiDung(newNguoiDung, (err, _nguoiDung)=>{
       if(err){
           res.json({
               success: false,
               msg: 'Them Nguoi Dung Moi That Bai'
           });
       }else {
           res.json({
               success: true,
               msg: 'Them Nguoi Dung Moi Thanh Cong',
               time: Date.now()
           });
       }
    });
});

//authenticate
router.post('/authenticate', (req, res, next)=>{
    const taiKhoan = req.body.taiKhoan;
    const matKhau = req.body.matKhau;
    nguoiDung.getNguoiDungByTaiKhoan(taiKhoan, (err, _nguoiDung)=>{
       if(err){
           throw err;
       }
       if(!_nguoiDung){
           return res.json({success: false, msg: 'Nguoi Dung Khong Ton Tai'});
       }
       console.log(_nguoiDung.dataType);
       nguoiDung.soSanhMatKhau(matKhau, _nguoiDung.matKhau, (err, isMatch)=>{
           if(err){
               throw err;
           }
           if(isMatch){
               const token = jwt.sign(_nguoiDung.toJSON(), databaseConfig.secret, {
                  expiresIn: 86400
               });
               res.json({
                  success: true,
                  token: 'JWT ' + token,
                   nguoiDung:{
                      id: _nguoiDung._id
                   }
               });
           }else {
               return res.json({success: false, msg: 'Sai Mat Khau'});
           }
       })
    });
});

//profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    res.send('dangky');
});

//validate
router.get('/validate', (req, res, next)=>{
    res.send('dangky');
});



module.exports = router;