const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const hangHoaSchema = mongoose.Schema({
    maHandHoa: {
        type: String,
        required: true
    },
    tuKhoa:{
        type: String,
        required: true
    },
    tenHangHoa:{
        type: String,
        required: true
    },
    thoiGianBaoHanh:{
        type: Number,
        required: true
    },
    moTa:{
        type: String,
        required: false
    },
    donVi:{
        type: String,
        required: false
    },
    danhGia:{
        /**
         * 5 = hang chinh hang thao xe
         * 6 = hang xin hon hang chinh hang
         * 4 = hang chat luong tot
         * 3 = hang chat luong binh thuong
         */
        type: Number,
        default: 0
    },
    giaBanLe_1:{
        type: Number,
        required: true
    },
    giaBanLe_2:{
        type: Number,
        default: 0
    },
    giaBanLe_3:{
        type: Number,
        default: 0
    },
    giabanBuon_1:{
        type: Number,
        default: true
    },
    giabanBuon_2:{
        type: Number,
        default: 0
    },
    giabanBuon_3:{
        type: Number,
        default: 0
    }
});