const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const nhapHandSchema = mongoose.Schema({
    maVach: {
        type: String,
        required: true
    },
    maHoaDon:{
        type: String,
        required: true
    },
    giaNhap:{
        type: Number,
        required: true
    },
    soLuong:{
        type: String,
        required: true
    },
    moTa:{
        type: String,
        required: false
    }
});