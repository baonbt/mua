const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const khuyenMaiSchema = mongoose.Schema({
    maKhuyenMai: {
        type: String,
        required: true
    },
    maHangHoa:{
        type: String,
        required: true
    },
    thoiGianBatDau:{
        type: Date,
        required: true
    },
    thoiGianKetThuc:{
        type: Date,
        required: true
    },
    tenKhuyenMai:{
        type: String,
        required: false
    },
    moTa:{
        type: String,
        required: false
    },
    giamGia:{
        type: Number,
        default: 0
    },
    tangThemSanPham:{
        type: Number,
        default: 0
    }
});