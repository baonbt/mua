const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const xuatHandSchema = mongoose.Schema({
    maVach: {
        type: String,
        required: true
    },
    maHoaDon:{
        type: String,
        required: true
    },
    giaXuat:{
        type: Number,
        required: true
    },
    soLuong:{
        type: String,
        required: true
    },
    moTa:{
        type: String,
        required: false
    }
});