const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const nguonHandSchema = mongoose.Schema({
    ten: {
        type: String,
        required: true
    },
    diaChi:{
        type: String,
        required: true
    },
    soDienThoai:{
        type: String,
        required: true
    },
    noCu:{
        type: Number,
        required: true
    },
    moTa:{
        type: String,
        required: false
    }
});