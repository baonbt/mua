const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const khachHandSchema = mongoose.Schema({
    tenKhachHand: {
        type: String,
        required: false
    },
    ngaySinh:{
        type: Date,
        required: false
    },
    diaChi:{
        type: String,
        required: false
    },
    soDienThoai:{
        type: String,
        required: false
    },
    matKhau:{
        type: String,
        required: true
    },
    moTa:{
        type: String,
        required: false
    },
    danhGia:{
        /**
         * toi da 5 diem
         */
        type: Number,
        default: 0
    },
    noCu:{
        type: Number,
        required: true
    }
});