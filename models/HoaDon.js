const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const hoaDonSchema = mongoose.Schema({
    maHoaDon: {
        type: String,
        required: true
    },
    thoiGianLap:{
        type: Date.now(),
        required: true
    },
    nguoiMua:{
        type: String,
        required: true
    },
    thanhToan:{
        type: Number,
        required: true
    },
    moTa:{
        type: String,
        required: false
    }
});