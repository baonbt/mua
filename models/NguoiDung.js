const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const databaseConfig = require('../config/Database');

//user schema
const nguoiDungSchema = mongoose.Schema({
    ten: {
        type: String,
        required: true
    },
    soDienThoai:{
        type: String,
        required: true
    },
    taiKhoan:{
        type: String,
        required: true
    },
    matKhau:{
        type: String,
        required: true
    },
    moTa:{
      type: String,
      required: false
    },
    capDo:{
        /**
         * 0 = 0
         * 1 = nhan vien thong thuong
         * 2 = truong nhom
         * 3 = quan ly
         */
        type: Number,
        default: 0
    },
    ngaySinh:{
        type: Number,
        required: false
    },
    phanLoai:{
        /**
         * 0 = nhan vien sua chua
         * 1 = nhan vien ban hang
         * 3 = lai xe
         * 4 = phu xe
         * 90-99
         */
        type: Number,
        default: 0,
    }
});

const nguoiDung = module.exports = mongoose.model('nguoidung', nguoiDungSchema);

module.exports.getNguoiDungById = function (id, callback) {
    nguoiDung.findById(id, callback);
};

module.exports.getNguoiDungByTaiKhoan = function (_taiKhoan, callback) {
    const query = {taiKhoan: _taiKhoan};
    nguoiDung.findOne(query, callback);
};

module.exports.addNguoiDung = function (newNguoiDung, callback) {
  bcrypt.genSalt(10, (err, salt)=>{
      bcrypt.hash(newNguoiDung.matKhau, salt, (err, hash)=>{
          if(err){
              throw err;
          }
          newNguoiDung.matKhau = hash;
          newNguoiDung.save(callback);
      });
  });
};

module.exports.soSanhMatKhau = function (candidateMatKhau, hash, callback) {
    bcrypt.compare(candidateMatKhau, hash, (err, isMatch)=>{
        if(err){
            throw err;
        }
        callback(null, isMatch);
    });
};