const jwtStrategy = require('passport-jwt').Strategy;
const extractJwt = require('passport-jwt').ExtractJwt;
const nguoiDung = require('../models/NguoiDung');
const databaseConfig = require('./Database');

module.exports = function (passport) {
    let options = {};
    options.jwtFromRequest = extractJwt.fromAuthHeaderWithScheme("jwt");
    options.secretOrKey = databaseConfig.secret;
    passport.use(new jwtStrategy(options, (jwt_payload, done)=>{
        console.log(jwt_payload);
        nguoiDung.getNguoiDungById(jwt_payload._id, (err, _nguoiDung)=>{
            if(err){
                return done(err, false);
            }
            if(_nguoiDung){
                return done(null, _nguoiDung);
            }else {
                return done(null, false);
            }
        });
    }));
};